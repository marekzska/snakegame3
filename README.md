# Serpent

Tento projekt bol inšpirovaný videotutoriálom of [@Yablka](https://www.youtube.com/watch?v=D-dtyO44ANA&t=3944s&ab_channel=ROBWEBsyablkom)

## Použité technológie

Projekt je postavený v Reacte a obsahuje Material UI dialog

## Stručný popis

Hracia plocha, had a jedlo sú štylizované divy.
Dokument zachytáva stláčanie šípok a na základe toho mení smer pohybu hada.
Had je reprezentovaný poľom objektov, každý objekt predstavuje jeden segment hada a obsahuje jeho súradnice v rámci hracej plochy.
Pohyb hada a jeho rast sú programované pomocou operácií s poľom. 

Ide o tretiu verziu tejto hry, v každej verzii som k problému pristupoval iným spôsobom, iba tento bol správny.

Nájdete tu: https://marekzska.itervitae.eu/Serpent/