import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Playground from './Playground';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Playground />
  </React.StrictMode>
);