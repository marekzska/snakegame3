import "./playground.css";
import Snake from "./Snake.js";
import { useState, useEffect } from "react";
import Food from "./Food";
import Alert from "./Alert";

export default function Playground() {
  useEffect(() => {
    if (localStorage.getItem("bestScore") === "") {
      localStorage.setItem("bestScore", "0");
    }
  }, []);
  const getRandomCoords = () => {
    let x = Math.floor((Math.random() * (96 - 1 + 1) + 1) / 4) * 4;
    let y = Math.floor((Math.random() * (96 - 1 + 1) + 1) / 4) * 4;
    return { x: x, y: y };
  };

  const [time, setTime] = useState(500);
  const [collision, setCollision] = useState(false);
  const [food, setFood] = useState(getRandomCoords);
  const [direction, setDirection] = useState("Right");
  const [score, setScore] = useState(0);
  const [snake, setSnake] = useState([
    { x: 0, y: 0 },
    { x: 4, y: 0 },
  ]);

  function handleKeyDown(e) {
    e.preventDefault();
    if (e.keyCode === 37) {
      setDirection("Left");
    }
    if (e.keyCode === 38) {
      setDirection("Up");
    }
    if (e.keyCode === 39) {
      setDirection("Right");
    }
    if (e.keyCode === 40) {
      setDirection("Down");
    }
  }

  const moveSnake = () => {
    let dots = [...snake];
    let head = dots[dots.length - 1];

    switch (direction) {
      case "Up":
        if (head.y === 0) {
          head = { ...head, x: head.x, y: 96 };
        } else {
          head = { ...head, x: head.x, y: head.y - 4 };
        }
        break;
      case "Down":
        if (head.y === 96) {
          head = { ...head, x: head.x, y: 0 };
        } else {
          head = { ...head, x: head.x, y: head.y + 4 };
        }
        break;
      case "Left":
        if (head.x === 0) {
          head = { ...head, x: 96, y: head.y };
        } else {
          head = { ...head, x: head.x - 4, y: head.y };
        }
        break;
      case "Right":
        if (head.x === 96) {
          head = { ...head, x: 0, y: head.y };
        } else {
          head = { ...head, x: head.x + 4, y: head.y };
        }
        break;
    }
    dots.push(head);
    dots.shift();
    setSnake(dots);
  };

  const collisionCheck = () => {
    let dots = [...snake];
    let head = dots[dots.length - 1];

    dots.pop();
    dots.map((item) => {
      if (head.x === item.x && head.y === item.y) {
        setCollision(true);
        setSnake([
          { x: 0, y: 0 },
          { x: 4, y: 0 },
        ]);
        setTime(500);
      }
    });
  };

  const snakeGrow = () => {
    let newSnake = [...snake];
    newSnake.unshift({});
    setSnake(newSnake);
    if (time !== 100) setTime((time) => time - 50);
  };

  const checkFood = () => {
    let head = snake[snake.length - 1];
    if (head.x === food.x && head.y === food.y) {
      snakeGrow();
      setScore((score) => score + 1);
      setFood(getRandomCoords);
    }
  };

  useEffect(() => {
    if(score > parseInt(localStorage.getItem('bestScore'))){
        localStorage.setItem("bestScore", score.toString());
    }
  }, [score]);

  useEffect(() => {
    document.onkeydown = handleKeyDown;
    const timer = setInterval(moveSnake, time);
    collisionCheck();
    checkFood();
    return () => {
      clearInterval(timer);
    };
  }, [snake, time]);

  return (
    <>
      <header>
        <h1 id='title'>Serpent</h1>
        <h2 id='score'>{score}</h2>
      </header>
      <div id='playground' tabIndex={-1}>
        <Snake snake={snake} />
        <Food food={food} />
        {collision && <Alert setScore={setScore} bestScore={localStorage.getItem("bestScore")} score={score} setCollision={setCollision} />}
      </div>
    </>
  );
}
